# BCG Test #

Build a Music Player App in Flutter

### Supported devices ###

* Android devices
* UI Tested on OPPO A5s

### Supported features ###

* Search music and play music, use the iTunes affiliate Api

### Requirements to build the app ###

* visual Studio Code or Android Stuido
* Dart & Flutter
* iTunes Api Search : https://itunes.apple.com/search?term=Dewa+19&media=music&sort=popularity&country=id&limit=20

### Instructions to build and deploy the app ###

* to Build added these pub files in pubspcae.yaml :
* http: 0.12.1
* audioplayers: ^0.15.1
* provider: 4.1.2
* flutter_spinkit: ^2.1.0
* flutter_vector_icons: ^0.2.1 
* flutter_launcher_icons: ^0.7.5
* google_fonts: ^1.1.0
* to Deploy : flutter clean & flutter build apk