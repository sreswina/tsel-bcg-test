import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:tian_musicplayer/model/listmusic_m.dart';
import 'package:tian_musicplayer/services/repo_provider.dart';
import 'package:tian_musicplayer/utils/colors_util.dart';
import 'package:provider/provider.dart';

class ListMusicsRowTemplate extends StatefulWidget {
  final Results radioModel;

  ListMusicsRowTemplate({Key key, this.radioModel}) : super(key: key);

  @override
  _ListMusicsRowTemplateState createState() => _ListMusicsRowTemplateState();
}

class _ListMusicsRowTemplateState extends State<ListMusicsRowTemplate> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildSongRow();
  }

  Widget _buildSongRow() {
    var playerProvider = Provider.of<PlayerProvider>(context, listen: false);
    playerProvider.resetStreams();
    bool _isSelectedRadio;
    if (playerProvider.currentRadio == null) {
      _isSelectedRadio = false;
    } else {
      _isSelectedRadio =
          this.widget.radioModel.trackId == playerProvider.currentRadio.trackId;
    }

    return ListTile(
      isThreeLine: true,
      title: Text(
        this.widget.radioModel.trackName,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
      leading: _image(this.widget.radioModel.artworkUrl100),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(this.widget.radioModel.artistName, maxLines: 2),
          Text(this.widget.radioModel.collectionName, maxLines: 2),
        ],
      ),
      trailing: Wrap(
        spacing: -10.0,
        runSpacing: 0.0,
        children: <Widget>[
          _buildPlayStopIcon(
            playerProvider,
            _isSelectedRadio,
          ),
        ],
      ),
    );
  }

  Widget _image(url, {size}) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image.network(url),
      ),
      height: size == null ? 55 : size,
      width: size == null ? 55 : size,
      decoration: BoxDecoration(
        color: ColorUtil("#FFE5EC"),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 3,
            offset: Offset(0, 3),
          ),
        ],
      ),
    );
  }

  Widget _buildPlayStopIcon(
      PlayerProvider playerProvider, bool _isSelectedSong) {
    return IconButton(
      icon: _buildAudioButton(playerProvider, _isSelectedSong),
      onPressed: () {
        if (!playerProvider.isStopped() && _isSelectedSong) {
          playerProvider.stopRadio();
        } else {
          if (!playerProvider.isLoading()) {
            playerProvider.initAudioPlugin();
            playerProvider.setAudioPlayer(this.widget.radioModel);

            playerProvider.playRadio();
          }
        }
      },
    );
  }

  getSpinkKit() {
    return SpinKitWave(
      color: Color(0xFFFF5252),
      size: 20.0,
    );
  }

  Widget _buildAudioButton(PlayerProvider model, _isSelectedSong) {
    if (_isSelectedSong) {
      if (model.isLoading()) {
        return Center(
          child: getSpinkKit(),
        );
      }

      if (!model.isStopped()) {
        return Icon(MaterialCommunityIcons.pause_circle,
            size: 30, color: Color(0xFFFF5252));
      }

      if (model.isStopped()) {
        return Icon(MaterialCommunityIcons.play_circle,
            size: 30, color: Color(0xFFFF5252));
      }
    } else {
      return Icon(MaterialCommunityIcons.play_circle,
          size: 30, color: Color(0xFFFF5252));
    }

    return Container();
  }
}
