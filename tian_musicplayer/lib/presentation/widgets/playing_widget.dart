import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:tian_musicplayer/services/repo_provider.dart';
import 'package:tian_musicplayer/utils/colors_util.dart';
import 'package:provider/provider.dart';

class NowPlayingTemplate extends StatelessWidget {
  final String radioTitle;
  final String radioImageURL;

  NowPlayingTemplate({Key key, this.radioTitle, this.radioImageURL})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(color: Color(0xFFFF6D00)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _nowPlayingText(context, this.radioTitle, this.radioImageURL),
            ],
          ),
        ),
      ),
    );
  }

  Widget _nowPlayingText(BuildContext context, String title, String imageURL) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 5, 10),
      child: ListTile(
        title: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: ColorUtil("#ffffff"),
          ),
        ),
        subtitle: Text(
          "Now Playing",
          textScaleFactor: 0.8,
          style: TextStyle(
            color: ColorUtil("#ffffff"),
          ),
        ),
        leading: _image(imageURL, size: 50.0),
        trailing: Wrap(
          spacing: -10.0,
          children: <Widget>[
            _buildStopIcon(context),
          ],
        ),
      ),
    );
  }

  Widget _image(url, {size}) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image.network(url),
      ),
      height: size == null ? 55 : size,
      width: size == null ? 55 : size,
      decoration: BoxDecoration(
        color: ColorUtil("#FFE5EC"),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 3,
            offset: Offset(0, 3),
          ),
        ],
      ),
    );
  }

  Widget _buildStopIcon(BuildContext context) {
    var playerProvider = Provider.of<PlayerProvider>(context, listen: false);

    return IconButton(
      icon: Icon(
        MaterialCommunityIcons.pause_circle,
        size: 30,
      ),
      color: ColorUtil("#ffffff"),
      onPressed: () {
        playerProvider.stopRadio();
      },
    );
  }
}
