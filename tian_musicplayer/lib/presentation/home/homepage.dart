import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:tian_musicplayer/presentation/widgets/playing_widget.dart';
import 'package:tian_musicplayer/presentation/widgets/row_widget.dart';
import 'package:tian_musicplayer/services/repo_provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _searchArtist = TextEditingController();
  Timer _turnon;

  @override
  void initState() {
    super.initState();
    var playerProvider = Provider.of<PlayerProvider>(context, listen: false);

    playerProvider.initAudioPlugin();
    playerProvider.resetStreams();
    playerProvider.getAllListMusics(searchKeyword: "Dewa 19");

    _searchArtist.addListener(_searchChanged);
  }

  _searchChanged() {
    var listMusicsBloc = Provider.of<PlayerProvider>(context, listen: false);

    if (_turnon?.isActive ?? false) _turnon.cancel();
    _turnon = Timer(const Duration(milliseconds: 500), () {
      listMusicsBloc.getAllListMusics(
        searchKeyword: _searchArtist.text,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final Color orange = Color(0xFFFF6F00);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFFFE0B2),
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "Music Player Take Home Test BCG",
            style: GoogleFonts.abel(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                textStyle: TextStyle(
                  color: Color(0xFFffffff),
                )),
          ),
          elevation: 0,
          backgroundColor: orange,
        ),
        body: Column(
          children: [_searchBar(), _radiosList(), _nowPlaying()],
        ),
      ),
    );
  }

  Widget _searchBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(Icons.search),
          Flexible(
            child: TextField(
              cursorColor: Colors.black,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(5),
                hintText: 'Search Artist',
              ),
              controller: _searchArtist,
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _radiosList() {
    return Consumer<PlayerProvider>(
      builder: (context, radioModel, child) {
        if (radioModel.allListMusic.length > 0) {
          return Expanded(
            child: Padding(
              child: ListView(
                children: <Widget>[
                  ListView.separated(
                      itemCount: radioModel.allListMusic.length,
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return ListMusicsRowTemplate(
                            radioModel: radioModel.allListMusic[index]);
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      })
                ],
              ),
              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
            ),
          );
        }

        // ignore: unrelated_type_equality_checks
        if (radioModel.allListMusic == 0) {
          return Expanded(
            child: _noData(),
          );
        }

        return getSpinkKit();
      },
    );
  }

  getSpinkKit() {
    return SpinKitFadingCircle(
      color: Color(0xFF8185E2),
      size: 30.0,
    );
  }

  Widget _noData() {
    String noDataTxt = "";
    bool showTextMessage = false;

    noDataTxt = "No Music List Found";
    showTextMessage = true;

    return Column(
      children: [
        Expanded(
          child: Center(
            child: showTextMessage
                ? Text(
                    noDataTxt,
                    textScaleFactor: 1,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                : getSpinkKit(),
          ),
        ),
      ],
    );
  }

  Widget _nowPlaying() {
    var playerProvider = Provider.of<PlayerProvider>(context, listen: true);
    playerProvider.resetStreams();
    String trackname;
    String trackImage;
    if (playerProvider.currentRadio == null) {
      trackname = "";
      trackImage = "";
    } else {
      trackname = playerProvider.currentRadio.trackName;
      trackImage = playerProvider.currentRadio.artworkUrl100;
    }
    return Visibility(
      visible: playerProvider.getPlayerState() == RadioPlayerState.PLAYING,
      child: NowPlayingTemplate(
        radioTitle: trackname,
        radioImageURL: trackImage,
      ),
    );
  }
}
