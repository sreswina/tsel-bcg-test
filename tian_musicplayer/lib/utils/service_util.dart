import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tian_musicplayer/model/listmusic_m.dart';

class UtilsService {
  Future<MusicListModel> getListData(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200) {
      return MusicListModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load data!');
    }
  }
}
