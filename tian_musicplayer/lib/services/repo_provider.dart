import 'dart:async';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:tian_musicplayer/model/listmusic_m.dart';
import 'package:tian_musicplayer/services/repo_fetch.dart';

enum RadioPlayerState { LOADING, STOPPED, PLAYING, PAUSED, COMPLETED }

class PlayerProvider with ChangeNotifier {
  AudioPlayer _audioPlayer;
  Results _listMusicsDetails;

  List<Results> _listMusicFetcher;
  List<Results> allListMusic;

  int get totalRecords =>
      _listMusicFetcher != null ? _listMusicFetcher.length : 0;

  Results get currentRadio => _listMusicsDetails;

  getPlayerState() => _playerState;

  getAudioPlayer() => _audioPlayer;

  getCurrentRadio() => _listMusicsDetails;

  RadioPlayerState _playerState = RadioPlayerState.STOPPED;
  StreamSubscription _positionSubscription;

  PlayerProvider() {
    _initStreams();
  }

  void _initStreams() {
    if (_listMusicsDetails == null) {
      if (_listMusicFetcher != null && _listMusicFetcher.length > 0) {
        _listMusicsDetails = _listMusicFetcher[0];
      }
    }
  }

  void resetStreams() {
    _initStreams();
  }

  void initAudioPlugin() {
    if (_playerState == RadioPlayerState.STOPPED) {
      _audioPlayer = new AudioPlayer();
    } else {
      _audioPlayer = getAudioPlayer();
    }
  }

  setAudioPlayer(Results music) async {
    _listMusicsDetails = music;

    await initAudioPlayer();
    notifyListeners();
  }

  initAudioPlayer() async {
    updatePlayerState(RadioPlayerState.LOADING);

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((Duration p) {
      if (_playerState == RadioPlayerState.LOADING && p.inMilliseconds > 0) {
        updatePlayerState(RadioPlayerState.PLAYING);
      }

      notifyListeners();
    });

    _audioPlayer.onPlayerStateChanged.listen((AudioPlayerState state) async {
      print("State : " + state.toString());
      if (state == AudioPlayerState.PLAYING) {
      } else if (state == AudioPlayerState.STOPPED ||
          state == AudioPlayerState.COMPLETED) {
        updatePlayerState(RadioPlayerState.STOPPED);
        notifyListeners();
      }
    });
  }

  playRadio() async {
    await _audioPlayer.play(currentRadio.previewUrl, stayAwake: true);
  }

  stopRadio() async {
    if (_audioPlayer != null) {
      _positionSubscription?.cancel();
      updatePlayerState(RadioPlayerState.STOPPED);
      await _audioPlayer.stop();
    }
    //await _audioPlayer.dispose();
  }

  bool isPlaying() {
    return getPlayerState() == RadioPlayerState.PLAYING;
  }

  bool isLoading() {
    return getPlayerState() == RadioPlayerState.LOADING;
  }

  bool isStopped() {
    return getPlayerState() == RadioPlayerState.STOPPED;
  }

  getAllListMusics({
    String searchKeyword = "",
  }) async {
    stopRadio();
    // ignore: deprecated_member_use
    _listMusicFetcher = List<Results>();
    // ignore: deprecated_member_use
    allListMusic = List<Results>();
    print('Search Keyword $searchKeyword');
    String url = "https://itunes.apple.com/search?term=" +
        searchKeyword +
        "&media=music" +
        "&sort=popularity" +
        "&country=id" +
        "&limit=20";

    print('Media Search Url : $url');
    MusicListModel model = await ListMusicsService.getAllMusics(url);

    if (model != null && model.results.length > 0) {
      var modellength = model.results.length.toString();
      print('Model Length : $modellength');

      allListMusic.addAll(model.results);
      var fetchmodellength = allListMusic.length.toString();
      print('All List Musics Length : $fetchmodellength');
    }
    notifyListeners();
  }

  void updatePlayerState(RadioPlayerState state) {
    _playerState = state;
    notifyListeners();
  }
}
