import 'package:tian_musicplayer/model/listmusic_m.dart';
import 'package:tian_musicplayer/utils/service_util.dart';

class ListMusicsService {
  static Future<MusicListModel> getAllMusics(String url) async {
    final serviceResponse = await UtilsService().getListData(url);
    return serviceResponse;
  }
}
